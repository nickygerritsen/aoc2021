import AoCShared

struct Day6: AdventOfCode {
    typealias InputData = [Int]

    func parse(input: String) -> [Int]? {
        input.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",").compactMap { Int($0) }
    }

    func solve(data: [Int], days: Int) -> Int {
        var numFish = Array<Int>(repeating: 0, count: 9)
        for fish in data {
            numFish[fish] += 1
        }

        for _ in 1...days {
            var newFish = numFish
            for i in 0...7 {
                newFish[i] = numFish[i+1]
            }
            newFish[8] = numFish[0]
            newFish[6] += numFish[0]
            numFish = newFish
        }

        return numFish.reduce(0) { $0 + $1 }
    }

    func part1(data: [Int]) {
        print(solve(data: data, days: 80))
    }

    func part2(data: [Int]) {
        print(solve(data: data, days: 256))
    }
}

Day6().solve()
