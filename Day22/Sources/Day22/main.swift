import AoCShared
import Regex

struct Point: Hashable {
    let x: Int
    let y: Int
    let z: Int
}

struct Cube: Hashable {

    let minPoint: Point
    let maxPoint: Point

    func intersect(other: Cube) -> Cube? {
        let minX = max(minPoint.x, other.minPoint.x)
        let maxX = min(maxPoint.x, other.maxPoint.x)
        let minY = max(minPoint.y, other.minPoint.y)
        let maxY = min(maxPoint.y, other.maxPoint.y)
        let minZ = max(minPoint.z, other.minPoint.z)
        let maxZ = min(maxPoint.z, other.maxPoint.z)

        if minX > maxX || minY > maxY || minZ > maxZ {
            return nil
        }

        return Cube(
                minPoint: Point(x: minX, y: minY, z: minZ),
                maxPoint: Point(x: maxX, y: maxY, z: maxZ)
        )
    }

    func subtract(other: Cube) -> Set<Cube> {
        // If the intersection is empty, the cubes do not overlap so we can return the original cube
        if intersect(other: other) == nil {
            return [self]
        }

        // Otherwise, we need to calculate the at most 27 (actually 26) cubes that result from slicing this cube into
        // parts based on the other cube
        var cubes: Set<Cube> = []

        // Limit the other cube to our dimensions, since we are only interested in that part
        let minX = max(minPoint.x, other.minPoint.x)
        let minY = max(minPoint.y, other.minPoint.y)
        let minZ = max(minPoint.z, other.minPoint.z)
        let maxX = min(maxPoint.x, other.maxPoint.x)
        let maxY = min(maxPoint.y, other.maxPoint.y)
        let maxZ = min(maxPoint.z, other.maxPoint.z)
        let other = Cube(
                minPoint: Point(x: minX, y: minY, z: minZ),
                maxPoint: Point(x: maxX, y: maxY, z: maxZ)
        )

        // Now loop over all 27 blocks that result in slicing the bigger cube into smaller ones on the lines of
        // the smaller one

        // Get all points of interest. Note that for the parts not inside the smaller cube, we need to offset by one
        let x1 = (minPoint.x, other.minPoint.x - 1)
        let x2 = (other.minPoint.x, other.maxPoint.x)
        let x3 = (other.maxPoint.x + 1, maxPoint.x)

        let y1 = (minPoint.y, other.minPoint.y - 1)
        let y2 = (other.minPoint.y, other.maxPoint.y)
        let y3 = (other.maxPoint.y + 1, maxPoint.y)

        let z1 = (minPoint.z, other.minPoint.z - 1)
        let z2 = (other.minPoint.z, other.maxPoint.z)
        let z3 = (other.maxPoint.z + 1, maxPoint.z)

        for x in [x1, x2, x3] {
            for y in [y1, y2, y3] {
                for z in [z1, z2, z3] {
                    // We never want to add the cube we want to subtract, i.e. the 'middle' one
                    if x == x2 && y == y2 && z == z2 {
                        continue
                    }

                    // Now add the cube if the volume is positive
                    let cube = Cube(
                            minPoint: Point(x: x.0, y: y.0, z: z.0),
                            maxPoint: Point(x: x.1, y: y.1, z: z.1)
                    )
                    if cube.volume > 0 {
                        cubes.insert(cube)
                    }
                }
            }
        }

        return cubes
    }

    var volume: Int {
        (maxPoint.x - minPoint.x + 1) * (maxPoint.y - minPoint.y + 1) * (maxPoint.z - minPoint.z + 1)
    }
}

struct Action {
    let cube: Cube
    let on: Bool
}

struct Day22: AdventOfCode {
    typealias InputData = [Action]

    func parse(input: String) -> [Action]? {
        let r = Regex("^(on|off) x=([0-9-]+)..([0-9-]+),y=([0-9-]+)..([0-9-]+),z=([0-9-]+)..([0-9-]+)$")
        return input.split(whereSeparator: \.isNewline).compactMap {
            guard let capture = r.firstMatch(in: String($0))?.captures,
                  let actionString = capture[0],
                  let minXString = capture[1],
                  let maxXString = capture[2],
                  let minYString = capture[3],
                  let maxYString = capture[4],
                  let minZString = capture[5],
                  let maxZString = capture[6],
                  let minX = Int(minXString),
                  let maxX = Int(maxXString),
                  let minY = Int(minYString),
                  let maxY = Int(maxYString),
                  let minZ = Int(minZString),
                  let maxZ = Int(maxZString) else {
                return nil
            }

            let on = actionString == "on"
            return Action(
                    cube: Cube(
                            minPoint: Point(x: minX, y: minY, z: minZ),
                            maxPoint: Point(x: maxX, y: maxY, z: maxZ)
                    ),
                    on: on
            )
        }
    }

    func calculate(data: [Action], intersectWith: Cube?) -> Int {
        var allCubes: Set<Cube> = []
        for action in data {
            let cube: Cube
            if let intersectWith = intersectWith {
                if let intersect = action.cube.intersect(other: intersectWith) {
                    cube = intersect
                } else {
                    continue
                }
            } else {
                cube = action.cube
            }

            // The first on cube we can just insert
            if allCubes.isEmpty && action.on {
                allCubes.insert(cube)
                continue
            }

            // Otherwise, determine which cubes are on
            // We first remove it from any existing cubes, since:
            // * If the new cube is on, we don't want it to be double on so we need to remove the overlap
            // * If the new cube is off, we want it to be off, so also remove it
            var newCubes: Set<Cube> = []
            for existingCube in allCubes {
                newCubes.formUnion(existingCube.subtract(other: cube))
            }

            // If the new cube is on, add it
            // If it is off, also add it
            if action.on {
                newCubes.insert(cube)
            }

            allCubes = newCubes
        }
        return allCubes.reduce(0) {
            $0 + $1.volume
        }
    }

    func part1(data: [Action]) {
        let smallCube = Cube(minPoint: Point(x: -50, y: -50, z: -50), maxPoint: Point(x: 50, y: 50, z: 50))
        print(calculate(data: data, intersectWith: smallCube))
    }

    func part2(data: [Action]) {
        print(calculate(data: data, intersectWith: nil))
    }
}

Day22().solve()
