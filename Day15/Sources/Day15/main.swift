import AoCShared

struct Grid {
    let risk: [[Int]]
}

struct Point: Hashable, Equatable {
    let x: Int
    let y: Int
}

struct Day15: AdventOfCode {
    typealias InputData = Grid

    func parse(input: String) -> Grid? {
        Grid(risk: input.split(whereSeparator: \.isNewline).compactMap {
            [Character]($0).compactMap {
                Int(String($0))
            }
        })
    }

    func distance(data: Grid) -> Int {
        var dist: [Point: Int] = [:]
        var Q = PriorityQueue<Point, Int>(comparator: <)

        let p0 = Point(x: 0, y: 0)
        dist[p0] = 0

        for y in 0..<data.risk.count {
            for x in 0..<data.risk[0].count {
                let p = Point(x: x, y: y)
                if p != p0 {
                    dist[p] = Int.max
                }

                Q.insert(element: p, value: dist[p]!)
            }
        }

        while Q.peek() != nil {
            let u = Q.pop()!

            let n = [
                Point(x: u.x - 1, y: u.y),
                Point(x: u.x + 1, y: u.y),
                Point(x: u.x, y: u.y - 1),
                Point(x: u.x, y: u.y + 1),
            ].filter {
                $0.x >= 0 && $0.x < data.risk[0].count && $0.y >= 0 && $0.y < data.risk.count
            }

            for v in n {
                let alt = dist[u]! + data.risk[v.y][v.x]
                if alt < dist[v]! {
                    dist[v] = alt
                    let p = Point(x: v.x, y: v.y)
                    Q.update(element: p, value: alt)
                }
            }
        }

        let p = Point(x: data.risk.count-1, y: data.risk[0].count - 1)

        return dist[p]!
    }

    func part1(data: Grid) {
        print(distance(data: data))
    }

    func part2(data: Grid) {
        let line = [Int](repeating: 0, count: data.risk[0].count * 5)
        var bigRisk = [[Int]](repeating: line, count: data.risk.count * 5)
        for y in 0..<data.risk.count * 5 {
            for x in 0..<data.risk[0].count * 5 {
                let yy = y - data.risk.count
                let xx = x - data.risk[0].count
                if y < data.risk.count && x < data.risk[0].count {
                    bigRisk[y][x] = data.risk[y][x]
                } else if y < data.risk.count {
                    bigRisk[y][x] = bigRisk[y][xx] + 1
                } else {
                    bigRisk[y][x] = bigRisk[yy][x] + 1
                }
                if bigRisk[y][x] > 9 {
                    bigRisk[y][x] = 1
                }
            }
        }

        print(distance(data: Grid(risk: bigRisk)))
    }
}

Day15().solve()
