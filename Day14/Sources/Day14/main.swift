import AoCShared
import Regex

struct Input {
    let template: String
    let replacements: [String: [String]]
}

struct Day14: AdventOfCode {
    typealias InputData = Input

    func parse(input: String) -> Input? {
        let lines = input.split(whereSeparator: \.isNewline)
        let template = String(lines[0])
        let r = Regex("^([A-Z]{2}) -> ([A-Z])$")
        var replacements: [String: [String]] = [:]
        for line in lines[1..<lines.count] {
            guard let captures = r.firstMatch(in: String(line))?.captures,
                  let from = captures[0],
                  let f1 = from.first,
                  let f2 = from.last,
                  let to = captures[1] else {
                return nil
            }

            replacements[from] = ["\(f1)\(to)", "\(to)\(f2)"]
        }

        return Input(template: template, replacements: replacements)
    }

    func initialCounts(data: Input) -> [String: Int] {
        var counts: [String: Int] = [:]

        let chars = [Character](data.template)
        for i in 0..<chars.count - 1 {
            let s = "\(chars[i])\(chars[i + 1])"
            counts[s] = (counts[s] ?? 0) + 1
        }

        return counts
    }

    func step(counts: [String: Int], data: Input) -> [String: Int] {
        var newCounts: [String: Int] = [:]
        for (s, count) in counts {
            guard let r = data.replacements[s] else {
                return newCounts
            }

            for rr in r {
                newCounts[rr] = (newCounts[rr] ?? 0) + count
            }
        }

        return newCounts
    }

    func counts(counts: [String: Int], data: Input) -> [Character: Int] {
        var charCounts: [Character: Int] = [:]
        guard let l = data.template.last else {
            return charCounts
        }
        // Last character is always there
        charCounts[l] = 1
        for (chars, count) in counts {
            guard let c = chars.first else {
                return charCounts
            }
            charCounts[c] = (charCounts[c] ?? 0) + count
        }

        return charCounts
    }

    func run(data: Input, numTimes: Int) {
        var c = initialCounts(data: data)
        for _ in 0..<numTimes {
            c = step(counts: c, data: data)
        }
        let cc = counts(counts: c, data: data)
        guard let minC = cc.values.min() else {
            return
        }
        guard let maxC = cc.values.max() else {
            return
        }
        print(maxC - minC)
    }

    func part1(data: Input) {
        run(data: data, numTimes: 10)
    }

    func part2(data: Input) {
        run(data: data, numTimes: 40)
    }
}

Day14().solve()
