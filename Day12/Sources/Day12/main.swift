import AoCShared

class Node {
    let name: String
    var edges: [String] = []
    var visited = false

    init(name: String) {
        self.name = name
    }

    var isStart: Bool {
        name == "start"
    }

    var isEnd: Bool {
        name == "end"
    }

    var isSmall: Bool {
        name[name.startIndex].isLowercase
    }
}

class Graph {
    var nodes: [String: Node]
    var allowSmallCaveTwice = false

    init() {
        nodes = [:]
    }

    func addNode(node: String) {
        if nodes.index(forKey: node) == nil {
            nodes[node] = Node(name: node)
        }
    }

    func addEdge(a: String, b: String) {
        addNode(node: a)
        addNode(node: b)
        guard let na = nodes[a] else {
            return
        }
        guard let nb = nodes[b] else {
            return
        }
        na.edges.append(b)
        nb.edges.append(a)
    }
}

struct Day12: AdventOfCode {
    typealias InputData = Graph

    func parse(input: String) -> Graph? {
        let g = Graph()
        for line in input.split(whereSeparator: \.isNewline) {
            let nodes = line.split(separator: "-")
            g.addEdge(a: String(nodes[0]), b: String(nodes[1]))
        }

        return g
    }

    func allPathsInner(data: Graph, from: String, to: String, paths: inout [String], allPaths: inout [[String]]) {

        guard let node = data.nodes[from] else {
            return
        }

        if node.isSmall {
            node.visited = true
        }

        paths.append(from)

        if from == to {
            allPaths.append(paths)
        } else {
            for e in node.edges {
                guard let n2 = data.nodes[e] else {
                    return
                }

                if n2.isSmall && data.allowSmallCaveTwice && !n2.isStart {
                    // Check if this can ever be a viable path. It can only be if it contains at most
                    // one small item twice
                    var counts = [String: Int]()
                    counts[e] = 1
                    for p in paths {
                        guard let pn = data.nodes[p] else {
                            return
                        }

                        if pn.isSmall && !pn.isStart && !pn.isEnd {
                            counts[p] = (counts[p] ?? 0) + 1
                        }
                    }
                    if counts.filter({$0.value == 2}).count <= 1 && counts.filter({$0.value > 2}).count == 0 {
                        allPathsInner(data: data, from: e, to: to, paths: &paths, allPaths: &allPaths)
                    }
                } else if !n2.visited {
                    allPathsInner(data: data, from: e, to: to, paths: &paths, allPaths: &allPaths)
                }
            }
        }

        paths.removeLast()
        node.visited = false
    }

    func allPaths(data: Graph) -> [[String]] {
        var paths = [String]()
        var allPaths = [[String]]()
        allPathsInner(data: data, from: "start", to: "end", paths: &paths, allPaths: &allPaths)
        return allPaths
    }

    func part1(data: Graph) {
        print(allPaths(data: data).count)
    }

    func part2(data: Graph) {
        data.allowSmallCaveTwice = true
        print(allPaths(data: data).count)
    }
}

Day12().solve()
