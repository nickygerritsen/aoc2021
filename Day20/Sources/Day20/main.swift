import AoCShared

enum Pixel: Character {
    case Light = "#"
    case Dark = "."
}

extension Pixel: CustomDebugStringConvertible {
    var debugDescription: String {
        "\(self.rawValue)"
    }
}

struct Point: Hashable, Equatable {
    let x: Int
    let y: Int
}

struct Trench {
    let algorithm: [Pixel]
    var image: [Point: Pixel]
    var minX: Int
    var minY: Int
    var maxX: Int
    var maxY: Int

    func pixelValue(point: Point, even: Bool) -> Pixel {
        if let pixel = image[point] {
            return pixel
        }

        if algorithm[0] == .Dark && algorithm[255] == .Light {
            return .Dark
        } else if even {
            return .Dark
        } else {
            return .Light
        }
    }

    var litCount: Int {
        image.filter {
            $0.value == .Light
        }.count
    }
}

struct Day20: AdventOfCode {
    typealias InputData = Trench

    func parse(input: String) -> Trench? {
        let parts = input.components(separatedBy: "\n\n")

        let algorithm = parts[0].compactMap {
            Pixel(rawValue: $0)
        }

        var image: [Point: Pixel] = [:]
        var y = 0
        var maxY = 0
        var maxX = 0
        for line in parts[1].split(whereSeparator: \.isNewline) {
            var x = 0
            for c in line {
                guard let pixel = Pixel(rawValue: c) else {
                    return nil
                }
                image[Point(x: x, y: y)] = pixel
                maxY = max(y, maxY)
                maxX = max(x, maxX)
                x += 1
            }
            y += 1
        }

        return Trench(algorithm: algorithm, image: image, minX: 0, minY: 0, maxX: maxX, maxY: maxY)
    }

    func mapPixels(algorithm: [Pixel], pixels: [Pixel]) -> Pixel {
        let n = pixels.map {
            $0 == .Dark ? "0" : "1"
        }.joined()
        return algorithm[Int(n, radix: 2)!]
    }

    func performStep(data: inout Trench, even: Bool) {
        // Every step we need to move 2 places out everywhere, since that is the amount that can change
        let newMinX = data.minX - 2
        let newMinY = data.minY - 2
        let newMaxX = data.maxX + 2
        let newMaxY = data.maxY + 2
        var newImage: [Point: Pixel] = [:]

        for x in newMinX...newMaxX {
            for y in newMinY...newMaxY {
                let points = [
                    Point(x: x - 1, y: y - 1),
                    Point(x: x, y: y - 1),
                    Point(x: x + 1, y: y - 1),
                    Point(x: x - 1, y: y),
                    Point(x: x, y: y),
                    Point(x: x + 1, y: y),
                    Point(x: x - 1, y: y + 1),
                    Point(x: x, y: y + 1),
                    Point(x: x + 1, y: y + 1),
                ]

                let pixels = points.map { data.pixelValue(point: $0, even: even)}
                let newPixel = mapPixels(algorithm: data.algorithm, pixels: pixels)
                let p = Point(x: x, y: y)
                newImage[p] = newPixel
            }
        }

        data.image = newImage
        data.minX = newMinX
        data.minY = newMinY
        data.maxX = newMaxX
        data.maxY = newMaxY
    }

    func performSteps(data: Trench, num: Int) -> Int {
        var data = data
        var even = true
        for _ in 0..<num {
            performStep(data: &data, even: even)
            even = !even
        }

        return data.litCount
    }

    func part1(data: Trench) {
        print(performSteps(data: data, num: 2))
    }

    func part2(data: Trench) {
        print(performSteps(data: data, num: 50))
    }
}

Day20().solve()
