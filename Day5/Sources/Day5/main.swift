import AoCShared
import Regex

struct Point {
    let x: Int
    let y: Int
}

extension Point: Hashable {
}

extension Point: Comparable {
    static func <(lhs: Point, rhs: Point) -> Bool {
        if lhs.x < rhs.x {
            return true
        } else if lhs.x > rhs.x {
            return false
        } else {
            return lhs.y < rhs.y
        }
    }
}

struct Line {
    let a: Point
    let b: Point

    init(a: Point, b: Point) {
        if a <= b {
            self.a = a
            self.b = b
        } else {
            self.b = a
            self.a = b
        }
    }

    var isHorizontal: Bool {
        a.y == b.y
    }

    var isVertical: Bool {
        a.x == b.x
    }
}

struct Day5: AdventOfCode {
    typealias InputData = [Line]

    func parse(input: String) -> [Line]? {
        let r = Regex("(\\d+),(\\d+) -> (\\d+),(\\d+)")
        return input.split(whereSeparator: \.isNewline).compactMap { line in
            guard let m = r.firstMatch(in: String(line)) else {
                return nil
            }

            guard let c1 = m.captures[0],
                  let c2 = m.captures[1],
                  let c3 = m.captures[2],
                  let c4 = m.captures[3],
                  let x1 = Int(c1),
                  let y1 = Int(c2),
                  let x2 = Int(c3),
                  let y2 = Int(c4) else {
                return nil
            }

            return Line(a: Point(x: x1, y: y1), b: Point(x: x2, y: y2))
        }
    }

    func solveGrid(lines: [Line]) -> Int {
        guard let maxx = lines.map({ $0.b.x }).max() else {
            return -1
        }
        guard let maxy = lines.map({ max($0.a.y, $0.b.y) }).max() else {
            return -1
        }

        var grid = (0...maxy).map { y in
            (0...maxx).map { x in
                0
            }
        }

        for line in lines {
            if line.isHorizontal {
                for x in line.a.x...line.b.x {
                    grid[line.a.y][x] += 1
                }
            } else if line.isVertical {
                for y in line.a.y...line.b.y {
                    grid[y][line.a.x] += 1
                }
            } else {
                // Diagonal line, determine which direction it goes.
                // Since a.x is always < b.x, it is going right, only thing we need to know is if we go up or down
                if line.b.y < line.a.y {
                    for (i, x) in (line.a.x...line.b.x).enumerated() {
                        grid[line.a.y - i][x] += 1
                    }
                } else {
                    for (i, x) in (line.a.x...line.b.x).enumerated() {
                        grid[line.a.y + i][x] += 1
                    }
                }
            }
        }

        return grid.reduce(0) { (count, row) in
            row.reduce(count) { (c, i) in
                c + (i > 1 ? 1 : 0)
            }
        }
    }

    func part1(data: [Line]) {
        print(solveGrid(lines: data.filter({ $0.isHorizontal || $0.isVertical })))
    }

    func part2(data: [Line]) {
        print(solveGrid(lines: data))
    }
}

Day5().solve()
