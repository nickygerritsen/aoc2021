import AoCShared

struct Day1: AdventOfCode {
    typealias InputData = [Int]

    func parse(input: String) -> [Int]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            Int($0)
        }
    }

    func part1(data: [Int]) {
        let items = zip(data, [-1] + data).map {
            ($0, $1)
        }
        print(items.filter({ $0.1 != -1 && $0.0 > $0.1 }).count)
    }

    func part2(data: [Int]) {
        let items = zip(data, zip([0] + data, [0, 0] + data)).map {
            ($0, $1.0, $1.1)
        }.dropFirst(2)
        let sums = items.map {
            $0.0 + $0.1 + $0.2
        }
        let summedItems = zip(sums, [-1] + sums).map {
            ($0, $1)
        }
        print(summedItems.filter({ $0.1 != -1 && $0.0 > $0.1 }).count)
    }
}

Day1().solve()
