import AoCShared

struct Point {
    let x: Int
    let y: Int
}

extension Point: Hashable {

}

// Source: https://github.com/raywenderlich/swift-algorithm-club/blob/master/Breadth-First%20Search/BreadthFirstSearch.playground/Sources/Queue.swift
struct Queue<T> {
    private var array: [T]

    public init() {
        array = []
    }

    public var isEmpty: Bool {
        array.isEmpty
    }

    public var count: Int {
        array.count
    }

    public mutating func enqueue(_ element: T) {
        array.append(element)
    }

    public mutating func dequeue() -> T? {
        if isEmpty {
            return nil
        } else {
            return array.removeFirst()
        }
    }

    public func peek() -> T? {
        array.first
    }
}

struct HeightMap {
    let data: [[Int]]
    let width: Int
    let height: Int

    init(data: [[Int]]) {
        self.data = data
        width = data[0].count
        height = data.count
    }

    func isLowPoint(p: Point) -> Bool {
        let possibleAdjacent = [
            Point(x: p.x - 1, y: p.y),
            Point(x: p.x + 1, y: p.y),
            Point(x: p.x, y: p.y - 1),
            Point(x: p.x, y: p.y + 1),
        ]
        let adjacent = possibleAdjacent.filter {
            $0.x >= 0 && $0.x < width && $0.y >= 0 && $0.y < height
        }

        return adjacent.allSatisfy {
            data[$0.y][$0.x] > data[p.y][p.x]
        }
    }

    func basinAround(p: Point) -> Set<Point> {
        // Use BFS
        var q = Queue<Point>()
        var s = Set<Point>()

        q.enqueue(p)
        s.insert(p)

        while let p = q.dequeue() {
            // Visit all neighbors
            let possibleAdjacent = [
                Point(x: p.x - 1, y: p.y),
                Point(x: p.x + 1, y: p.y),
                Point(x: p.x, y: p.y - 1),
                Point(x: p.x, y: p.y + 1),
            ]
            let adjacent = possibleAdjacent.filter {
                $0.x >= 0 && $0.x < width && $0.y >= 0 && $0.y < height
            }
            for r in adjacent {
                // r is part of the basin if:
                // - r is not 9; and
                // - data[r] is bigger than data[p]
                // But if r is already part of the basin, do not check it again
                if !s.contains(r) && data[r.y][r.x] != 9 && data[r.y][r.x] > data[p.y][p.x] {
                    s.insert(r)
                    // Since r is part of the basin, also check its neighbors
                    q.enqueue(r)
                }
            }
        }

        return s
    }
}

struct Day9: AdventOfCode {
    typealias InputData = HeightMap

    func parse(input: String) -> HeightMap? {
        HeightMap(data: input.split(whereSeparator: \.isNewline).compactMap {
            $0.compactMap {
                Int(String($0))
            }
        })
    }

    func lowPoints(data: HeightMap) -> [Point] {
        var result = [Point]()
        for x in 0..<data.width {
            for y in 0..<data.height {
                if data.isLowPoint(p: Point(x: x, y: y)) {
                    result.append(Point(x: x, y: y))
                }
            }
        }

        return result
    }

    func part1(data: HeightMap) {
        print(
                lowPoints(data: data)
                        .reduce(0) {
                            $0 + 1 + data.data[$1.y][$1.x]
                        }
        )
    }

    func part2(data: HeightMap) {
        let basinSizes = lowPoints(data: data)
                .map {
                    data.basinAround(p: $0)
                }
                .map {
                    $0.count
                }
                .sorted()
        print(
                basinSizes[0...2].reduce(1) {
                    $0 * $1
                }
        )
    }
}

Day9().solve()
