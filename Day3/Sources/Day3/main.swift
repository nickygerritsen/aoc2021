import AoCShared

struct Day3: AdventOfCode {
    typealias InputData = [[Character]]

    func parse(input: String) -> [[Character]]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            Array<Character>(String($0))
        }
    }

    func counts(data: [[Character]]) -> [(zero: Int, one: Int)] {
        let initial = data[0].map { _ in
            (zero: 0, one: 0)
        }
        return data.reduce(initial) { current, new in
            var c = current
            for (pos, item) in new.enumerated() {
                if item == "0" {
                    c[pos].zero += 1
                } else {
                    c[pos].one += 1
                }
            }

            return c
        }
    }

    func part1(data: [[Character]]) {
        let counts = counts(data: data)

        guard let gamma = Int(String(counts.map {
            $0.zero > $0.one ? "0" : "1"
        }), radix: 2) else {
            return
        }
        guard let epsilon = Int(String(counts.map {
            $0.zero < $0.one ? "0" : "1"
        }), radix: 2) else {
            return
        }

        print(gamma * epsilon)
    }

    func findByCriteria(data: [[Character]], cmp: ((zero: Int, one: Int)) -> Character) -> Int? {
        var data = data
        var currentBit = 0
        while data.count > 1 {
            let counts = counts(data: data)
            let bitToKeep = cmp(counts[currentBit])
            data = data.filter {
                $0[currentBit] == bitToKeep
            }
            currentBit += 1
        }

        return Int(String(data[0]), radix: 2)
    }

    func part2(data: [[Character]]) {
        guard let oxygen = findByCriteria(data: data, cmp: {
            $0.one >= $0.zero ? "1" : "0"
        }) else {
            return
        }
        guard let o2 = findByCriteria(data: data, cmp: {
            $0.zero <= $0.one ? "0" : "1"
        }) else {
            return
        }

        print(oxygen * o2)
    }
}

Day3().solve()
