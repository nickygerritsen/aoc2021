import AoCShared

struct State: Hashable, Equatable {
    var hallway: [Int] // What is in the hallway
    var rooms: [[Int]] // What is in the rooms
}

extension State: CustomDebugStringConvertible {
    var debugDescription: String {
        var s = "\n#############\n"
        s += "#"

        for h in hallway {
            switch h {
            case 1: s += "A"
            case 10: s += "B"
            case 100: s += "C"
            case 1000: s += "D"
            default: s += "."
            }
        }
        s += "#\n"

        s += "###"
        for r in rooms[0] {
            switch r {
            case 1: s += "A"
            case 10: s += "B"
            case 100: s += "C"
            case 1000: s += "D"
            default: s += "."
            }
            s += "#"
        }
        s += "##\n"

        s += "  #"
        for rr in 1..<rooms.count {
            for r in rooms[rr] {
                switch r {
                case 1: s += "A"
                case 10: s += "B"
                case 100: s += "C"
                case 1000: s += "D"
                default: s += "."
                }
                s += "#"
            }
            s += "  \n"
        }

        s += "  #########  "

        return s
    }
}

// Where we want to end up
let wanted1 = State(hallway: [Int](repeating: 0, count: 11), rooms: [[1, 10, 100, 1000], [1, 10, 100, 1000]])
let wanted2 = State(hallway: [Int](repeating: 0, count: 11), rooms: [[1, 10, 100, 1000], [1, 10, 100, 1000], [1, 10, 100, 1000], [1, 10, 100, 1000]])

struct Move {
    let state: State
    let cost: Int
}

struct CanMoveToRoomArgument: Hashable {
    let item: Int
    let hallwayPosition: Int
    let state: State
}

struct CanMoveToHallwayArgument: Hashable {
    let item: Int
    let hallwayPosition: Int
    let state: State
    let roomRow: Int
    let roomIndex: Int
}

struct ValidationResult {
    let valid: Bool
    let cost: Int
    let newState: State
}

var canMoveToRoomCache: [CanMoveToRoomArgument: ValidationResult] = [:]
var canMoveToHallwayCache: [CanMoveToHallwayArgument: ValidationResult] = [:]
var movesCache: [State: [Move]] = [:]
var fastestMoveCache: [State: Int] = [:]

let roomConnectionPositions = [2, 4, 6, 8]
let roomConnections: Set<Int> = Set(roomConnectionPositions)

struct Day23: AdventOfCode {
    typealias InputData = State

    func parse(input: String) -> State? {
        // How much each thing costs
        let costs = ["A": 1, "B": 10, "C": 100, "D": 1000, ".": 0]
        let lines = input.split(whereSeparator: \.isNewline)
        let hallway = lines[1].compactMap {
            costs[String($0)]
        }
        let room1 = lines[2].compactMap {
            costs[String($0)]
        }
        let room2 = lines[3].compactMap {
            costs[String($0)]
        }

        return State(hallway: hallway, rooms: [room1, room2])
    }

    func roomForItem(item: Int) -> Int {
        switch item {
        case 1: return 0
        case 10: return 1
        case 100: return 2
        case 1000: return 3
        default: return -1 // Won't be called
        }
    }

    func roomPosition(room: Int) -> Int {
        switch room {
        case 1: return 2
        case 10: return 4
        case 100: return 6
        case 1000: return 8
        default: return -1 // Won't be called
        }
    }

    func canMoveToRoom(arg: CanMoveToRoomArgument) -> ValidationResult {
        // Get the contest of the room where this item should go
        let roomContents = arg.state.rooms.map {
            $0[roomForItem(item: arg.item)]
        }
        // Check if all spots of the room are empty or contain the correct item
        let allValid = roomContents.allSatisfy {
            $0 == 0 || $0 == arg.item
        }
        if allValid {
            let roomPos = roomPosition(room: arg.item)
            var roomHeight = 0
            var cost = 1 + abs(arg.hallwayPosition - roomPos)
            for i in 1..<roomContents.count {
                if roomContents[i] == 0 {
                    cost += 1
                    roomHeight += 1
                } else {
                    break
                }
            }
            cost *= arg.item
            if arg.hallwayPosition > roomPos {
                // The item is to the right of the room, check if everything between index and roomPos is free
                let allFree = arg.state.hallway[roomPos..<arg.hallwayPosition].allSatisfy {
                    $0 == 0
                }
                if allFree {
                    var newState = arg.state
                    newState.hallway[arg.hallwayPosition] = 0
                    newState.rooms[roomHeight][roomForItem(item: arg.item)] = arg.item
                    return ValidationResult(valid: allFree, cost: cost, newState: newState)
                }
            } else {
                // The item is to the right of the room, check if everything between index and roomPos is free
                let allFree = arg.state.hallway[arg.hallwayPosition+1...roomPos].allSatisfy {
                    $0 == 0
                }
                if allFree {
                    var newState = arg.state
                    newState.hallway[arg.hallwayPosition] = 0
                    newState.rooms[roomHeight][roomForItem(item: arg.item)] = arg.item
                    return ValidationResult(valid: allFree, cost: cost, newState: newState)
                }
            }
        }

        return ValidationResult(valid: false, cost: 0, newState: arg.state)
    }

    func canMoveToHallway(arg: CanMoveToHallwayArgument) -> ValidationResult {
        // We can move to the hallway if there are no things on the way
        // All other checks have been done already
        let cost = (arg.roomRow + 1 + abs(arg.hallwayPosition - roomConnectionPositions[arg.roomIndex])) * arg.item
        let lo = min(roomConnectionPositions[arg.roomIndex], arg.hallwayPosition)
        let hi = max(roomConnectionPositions[arg.roomIndex], arg.hallwayPosition)

        let allFree = arg.state.hallway[lo...hi].allSatisfy {
            $0 == 0
        }

        if allFree {
            var newState = arg.state
            newState.rooms[arg.roomRow][arg.roomIndex] = 0
            newState.hallway[arg.hallwayPosition] = arg.item
            return ValidationResult(valid: true, cost: cost, newState: newState)
        }

        return ValidationResult(valid: false, cost: 0, newState: arg.state)
    }

    // Get all valid moves from the given state
    func moves(state: State) -> [Move] {
        var m: [Move] = []
        for (index, item) in state.hallway.enumerated() {
            // If there is something in the hallway, see if we can move it
            if item > 0 {
                let arg = CanMoveToRoomArgument(item: item, hallwayPosition: index, state: state)
                let result = cached(t: arg, f: canMoveToRoom, cache: &canMoveToRoomCache)
                if result.valid {
                    // If we can move something into its room, always do it since it is the fastest thing to do
                    return [Move(state: result.newState, cost: result.cost)]
                }
            }
        }

        var somethingAbove = [Bool](repeating: false, count: 4)
        for (roomRow, room) in state.rooms.enumerated() {
            for (roomIndex, item) in room.enumerated() {
                let roomPos = roomForItem(item: item)
                if item == 0 {
                    // There is nothing here, we can't move it
                    continue
                }

                // There is something above us, we can't move it
                if somethingAbove[roomIndex] {
                    continue
                }

                // Keep track of if there is something above us
                somethingAbove[roomIndex] = somethingAbove[roomIndex] || item > 0

                if roomPos == roomIndex && (roomRow + 1..<state.rooms.count).allSatisfy({ state.rooms[$0][roomIndex] == item }) {
                    // Item is already at the correct spot and all items below are as well
                    continue
                }

                for hallwayPos in 0..<11 {
                    if roomConnections.contains(hallwayPos) {
                        // Never place something in front of the room
                        continue
                    }

                    if state.hallway[hallwayPos] > 0 {
                        // We can't move it if there is something in the room
                        continue
                    }

                    let arg = CanMoveToHallwayArgument(item: item, hallwayPosition: hallwayPos, state: state, roomRow: roomRow, roomIndex: roomIndex)
                    let result = cached(t: arg, f: canMoveToHallway, cache: &canMoveToHallwayCache)
                    if result.valid {
                        m.append(Move(state: result.newState, cost: result.cost))
                    }
                }
            }
        }

        return m
    }

    func fastestMove(state: State, wanted: State) -> Int {
        if state == wanted {
            return 0
        } else {
            let possibleMoves = cached(t: state, f: moves, cache: &movesCache)
            return possibleMoves.map {
                if $0.cost == Int.max {
                    return Int.max
                }
                let fm = fastestMoveCached(state: $0.state, wanted: wanted)
                if fm == Int.max {
                    return Int.max
                }
                return fm + $0.cost
            }.min() ?? Int.max
        }
    }

    func fastestMoveCached(state: State, wanted: State) -> Int {
        if let c = fastestMoveCache[state] {
            return c
        }

        let c = fastestMove(state: state, wanted: wanted)
        fastestMoveCache[state] = c
        return c
    }

    func cached<T, U>(t: T, f: (T) -> U, cache: inout [T: U]) -> U {
        if let c = cache[t] {
            return c
        }

        let c = f(t)
        cache[t] = c
        return c
    }

    func part1(data: State) {
        print(fastestMove(state: data, wanted: wanted1))
    }

    func part2(data: State) {
        canMoveToRoomCache = [:]
        canMoveToHallwayCache = [:]
        movesCache = [:]
        fastestMoveCache = [:]
        let state = State(hallway: data.hallway, rooms: [data.rooms[0], [1000, 100, 10, 1], [1000, 10, 1, 100], data.rooms[1]])
        print(fastestMove(state: state, wanted: wanted2))
    }
}

Day23().solve()
