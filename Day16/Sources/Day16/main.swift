import AoCShared

enum Packet {
    case Literal(version: Int, type: Int, number: Int)
    case Operator(version: Int, type: Int, subPackets: [Packet])

    var versionSum: Int {
        switch self {
        case .Literal(version: let version, type: _, number: _):
            return version
        case .Operator(version: let version, type: _, subPackets: let subPackets):
            return subPackets.reduce(version) {
                $0 + $1.versionSum
            }
        }
    }

    var value: Int {
        switch self {
        case .Literal(version: _, type: _, number: let number):
            return number
        case .Operator(version: _, type: let type, subPackets: let subPackets):
            switch type {
            case 0:
                // Sum
                return subPackets.reduce(0) {
                    $0 + $1.value
                }
            case 1:
                // Product
                return subPackets.reduce(1) {
                    $0 * $1.value
                }
            case 2:
                // Minimum
                return subPackets.map {
                    $0.value
                }.min()!
            case 3:
                // Maximum
                return subPackets.map {
                    $0.value
                }.max()!
            case 5:
                // Greater than
                if subPackets[0].value > subPackets[1].value {
                    return 1
                }
                return 0
            case 6:
                // Less than
                if subPackets[0].value < subPackets[1].value {
                    return 1
                }
                return 0
            case 7:
                // Equal
                if subPackets[0].value == subPackets[1].value {
                    return 1
                }
                return 0
            default:
                print("Invalid type")
                return -1
            }
        }
    }
}

struct Day16: AdventOfCode {
    typealias InputData = Packet

    func parse(input: String) -> Packet? {
        let bits = input.trimmingCharacters(in: .whitespacesAndNewlines).compactMap { (c: Character) -> String? in
            guard let number = Int(String(c), radix: 16) else {
                return nil
            }

            let bits = String(number, radix: 2)
            let nbits = bits.count

            return (0..<(4 - nbits)).map { (_: Int) -> String in
                "0"
            }.joined() + bits
        }.flatMap { (s: String) -> [String] in
            s.map {
                String($0)
            }
        }

        return parsePackets(data: bits)
    }

    func readNumber(data: [String], pos: inout Int, length: Int) -> Int? {
        let n = Int(data[pos..<(pos + length)].joined(), radix: 2)
        pos += length
        return n
    }

    func parsePacket(data: [String], pos: inout Int) -> Packet? {
        guard let version = readNumber(data: data, pos: &pos, length: 3) else {
            return nil
        }
        guard let type = readNumber(data: data, pos: &pos, length: 3) else {
            return nil
        }
        if type == 4 {
            // Literal: keep reading 5 bits
            var numberBits = [String]()
            var hasNext = 1
            repeat {
                guard let hn = readNumber(data: data, pos: &pos, length: 1) else {
                    return nil
                }
                hasNext = hn
                numberBits += data[pos..<(pos + 4)]
                pos += 4
            } while hasNext == 1

            guard let number = Int(numberBits.joined(), radix: 2) else {
                return nil
            }

            return .Literal(version: version, type: type, number: number)
        } else {
            // Operator, parse the length type ID first
            guard let lengthTypeId = readNumber(data: data, pos: &pos, length: 1) else {
                return nil
            }

            if lengthTypeId == 0 {
                // Sub packets with total length
                guard let subPacketLength = readNumber(data: data, pos: &pos, length: 15) else {
                    return nil
                }

                let subPacketsStartPos = pos
                var subPackets = [Packet]()
                while pos < subPacketsStartPos + subPacketLength {
                    guard let subPacket = parsePacket(data: data, pos: &pos) else {
                        print("Can not read sub packet")
                        return nil
                    }
                    subPackets.append(subPacket)
                }

                return .Operator(version: version, type: type, subPackets: subPackets)
            } else {
                // Sub packet count
                guard let subPacketCount = readNumber(data: data, pos: &pos, length: 11) else {
                    return nil
                }

                var subPackets = [Packet]()
                for _ in 0..<subPacketCount {
                    guard let subPacket = parsePacket(data: data, pos: &pos) else {
                        print("Can not read sub packet")
                        return nil
                    }
                    subPackets.append(subPacket)
                }

                return .Operator(version: version, type: type, subPackets: subPackets)
            }
        }
    }

    func parsePackets(data: [String]) -> Packet? {
        var pos = 0
        let packet = parsePacket(data: data, pos: &pos)

        // Make sure we only have zeroes left
        guard let left = Int(data[pos..<data.count].joined(), radix: 2) else {
            return nil
        }

        if left != 0 {
            print("More than zeroes left!")
            return nil
        }

        return packet
    }

    func part1(data: Packet) {
        print(data.versionSum)
    }

    func part2(data: Packet) {
        print(data.value)
    }
}

Day16().solve()
