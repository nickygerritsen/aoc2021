import AoCShared

struct Day7: AdventOfCode {
    typealias InputData = [Int]

    func parse(input: String) -> [Int]? {
        input.trimmingCharacters(in: .whitespacesAndNewlines).split(separator: ",").compactMap {
            Int($0)
        }
    }

    func da(a: Int, b: Int) -> Int {
        abs(a - b)
    }

    func db(a: Int, b: Int) -> Int {
        abs(a - b) * (abs(a - b) + 1) / 2
    }

    func distance(data: [Int], n: Int, d: (Int, Int) -> Int) -> Int {
        data.reduce(0) {
            $0 + d(n, $1)
        }
    }

    func totalDistance(data: [Int], d: (Int, Int) -> Int) -> Int? {
        guard let minNum = data.min() else {
            return nil
        }
        guard let maxNum = data.max() else {
            return nil
        }

        return (minNum...maxNum).map({ distance(data: data, n: $0, d: d)}).min()
    }

    func part1(data: [Int]) {
        guard let closest = totalDistance(data: data, d: da) else {
            return
        }

        print(closest)
    }

    func part2(data: [Int]) {
        guard let closest = totalDistance(data: data, d: db) else {
            return
        }

        print(closest)
    }
}

Day7().solve()
