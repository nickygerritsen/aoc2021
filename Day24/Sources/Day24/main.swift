import AoCShared

enum Operation {
    case Inp(a: String)
    case Add(a: String, b: String, bi: Int?)
    case Mul(a: String, b: String, bi: Int?)
    case Div(a: String, b: String, bi: Int?)
    case Mod(a: String, b: String, bi: Int?)
    case Eql(a: String, b: String, bi: Int?)

    static func from(parts: [String]) -> Operation? {
        switch parts[0] {
        case "inp":
            return Inp(a: parts[1])
        case "add":
            return Add(a: parts[1], b: parts[2], bi: Int(parts[2]))
        case "mul":
            return Mul(a: parts[1], b: parts[2], bi: Int(parts[2]))
        case "div":
            return Div(a: parts[1], b: parts[2], bi: Int(parts[2]))
        case "mod":
            return Mod(a: parts[1], b: parts[2], bi: Int(parts[2]))
        case "eql":
            return Eql(a: parts[1], b: parts[2], bi: Int(parts[2]))
        default:
            return nil
        }
    }
}

struct Equation {
    let left: Int
    let right: Int
    let diff: Int
}

struct Day24: AdventOfCode {
    func parse(input: String) -> [Operation]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            Operation.from(parts: $0.split(separator: " ").map {
                String($0)
            })
        }
    }

    func getEquations(data: [Operation]) -> [Equation] {
        var numbers: [(offset: Int, check: Int, index: Int)] = []
        var equations: [Equation] = []
        for (i, x) in (0..<14).enumerated() {
            guard case let .Add(a: _, b: _, bi: check) = data[x * 18 + 5],
                  let check = check else {
                return []
            }
            guard case let .Add(a: _, b: _, bi: offset) = data[x * 18 + 15],
                  let offset = offset else {
                return []
            }

            let push = check > 0
            if push {
                numbers.append((offset: offset, check: check, index: i))
            } else {
                guard let n = numbers.popLast() else {
                    return []
                }
                let equation = Equation(left: i, right: n.index, diff: check + n.offset)
                equations.append(equation)
            }
        }

        return equations
    }

    func part1(data: [Operation]) {
        var digits = [Int](repeating: -1, count: 14)
        for e in getEquations(data: data) {
            if e.diff < 0 {
                // Right digit is 9, left digit is 9 + diff
                digits[e.right] = 9
                digits[e.left] = 9 + e.diff
            } else {
                // Left digit is 9, right digit is 9 - diff
                digits[e.left] = 9
                digits[e.right] = 9 - e.diff
            }
        }

        guard let n = Int(digits.map {
            String($0)
        }.joined()) else {
            return
        }
        print(n)
    }

    func part2(data: [Operation]) {
        var digits = [Int](repeating: -1, count: 14)
        for e in getEquations(data: data) {
            if e.diff < 0 {
                // Left digit is 1, right digit is 1 - diff
                digits[e.left] = 1
                digits[e.right] = 1 - e.diff
            } else {
                // Right digit is 9, left digit is 1 + diff
                digits[e.right] = 1
                digits[e.left] = 1 + e.diff
            }
        }

        guard let n = Int(digits.map {
            String($0)
        }.joined()) else {
            return
        }
        print(n)
    }

    typealias InputData = [Operation]
}

Day24().solve()
