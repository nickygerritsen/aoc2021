import AoCShared

struct Point {
    let x: Int
    let y: Int
}

extension Point: Hashable {
}

extension Point: Equatable {
}

struct Grid {
    var levels: [[Int]]

    mutating func step() -> Int {
        var flashes = 0
        var newLevels = levels
        for x in 0..<levels[0].count {
            for y in 0..<levels.count {
                newLevels[y][x] += 1
            }
        }

        var flashed = Set<Point>()
        var newFlashes = 0
        repeat {
            newFlashes = 0
            for x in 0..<levels[0].count {
                for y in 0..<levels.count {
                    if newLevels[y][x] > 9 {
                        // We are going to flash, but only if we did not flash yet
                        let p = Point(x: x, y: y)
                        if !flashed.contains(p) {
                            newFlashes += 1
                            flashed.insert(p)

                            let n = [
                                Point(x: -1, y: -1),
                                Point(x: 0, y: -1),
                                Point(x: 1, y: -1),
                                Point(x: -1, y: 0),
                                Point(x: 1, y: 0),
                                Point(x: -1, y: 1),
                                Point(x: 0, y: 1),
                                Point(x: 1, y: 1),
                            ]

                            let pp = n.map {
                                Point(x: p.x + $0.x, y: p.y + $0.y)
                            }

                            let npp = pp.filter {
                                $0.x >= 0 && $0.x < levels[0].count && $0.y >= 0 && $0.y < levels.count
                            }

                            for np in npp {
                                newLevels[np.y][np.x] += 1
                            }
                        }
                    }
                }
            }
            flashes += newFlashes
        } while newFlashes > 0

        for p in flashed {
            newLevels[p.y][p.x] = 0
        }

        levels = newLevels

        return flashes
    }
}

struct Day11: AdventOfCode {
    typealias InputData = Grid

    func parse(input: String) -> Grid? {
        Grid(levels: input.split(whereSeparator: \.isNewline).compactMap {
            $0.compactMap {
                Int(String($0))
            }
        })
    }

    func part1(data: Grid) {
        var data = data
        var c = 0
        for _ in 0..<100 {
            c += data.step()
        }

        print(c)
    }

    func part2(data: Grid) {
        var data = data
        var step = 1
        while true {
            let s = data.step()
            if s == data.levels[0].count * data.levels.count {
                print(step)
                return
            }
            step += 1
        }
    }
}

Day11().solve()
