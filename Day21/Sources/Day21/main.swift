import AoCShared
import Regex

struct DeterministicDie {
    var numThrows = 0
    var currentValue = 1

    mutating func throwTimes(n: Int) -> Int {
        numThrows += n
        let r = (currentValue + currentValue + n - 1) * n / 2
        currentValue += n
        return r
    }
}

class Player {
    let n: Int
    var pos: Int
    var score: Int = 0

    init(n: Int, pos: Int) {
        self.n = n
        self.pos = pos
    }

    func moveForward(toAdd: Int) {
        pos = (pos + toAdd) % 10
        if pos == 0 {
            pos = 10
        }
        score += pos
    }
}

struct State: Hashable {
    let player1Pos: Int
    let player2Pos: Int
    let player1Score: Int
    let player2Score: Int
    let player1Turn: Bool
}

struct WinTimes {
    var player1: Int
    var player2: Int
}

struct Day21: AdventOfCode {
    typealias InputData = [Player]

    func parse(input: String) -> [Player]? {
        let r = Regex("^Player (\\d+) starting position: (\\d+)$")

        return input.split(whereSeparator: \.isNewline).compactMap {
            guard let capture = r.firstMatch(in: String($0))?.captures,
                  let playerString = capture[0],
                  let positionString = capture[1],
                  let player = Int(playerString),
                  let position = Int(positionString)
                    else {
                return nil
            }

            return Player(n: player, pos: position)
        }
    }

    func part1(data: [Player]) {
        let data = data.map { Player(n: $0.n, pos: $0.pos) }
        var playerIndex = 0
        var dice = DeterministicDie()
        while true {
            let toAdd = dice.throwTimes(n: 3)

            data[playerIndex].moveForward(toAdd: toAdd)

            if data[playerIndex].score >= 1000 {
                break
            }

            playerIndex += 1
            if playerIndex >= data.count {
                playerIndex = 0
            }
        }

        guard let nonWinningPlayer = data.first(where: { $0.score < 1000 }) else {
            return
        }
        print(nonWinningPlayer.score * dice.numThrows)
    }

    func playDiracGames(player1: Player, player2: Player, player1Turn: Bool, forPlayer1: Bool, cache: inout [State: WinTimes]) -> WinTimes {
        let state = State(player1Pos: player1.pos, player2Pos: player2.pos, player1Score: player1.score, player2Score: player2.score, player1Turn: player1Turn)
        if let cached = cache[state] {
            return cached
        }
        // Array and not dict for major speed improvement
        let chances = [
            0,
            0,
            0,
            1,
            3,
            6,
            7,
            6,
            3,
            1,
        ]

        if player1.score >= 21 {
            return WinTimes(player1: 1, player2: 0)
        } else if player2.score >= 21 {
            return WinTimes(player1: 0, player2: 1)
        }

        let currentPlayer = player1Turn ? player1 : player2

        var winTimes = WinTimes(player1: 0, player2: 0)
        for outcome in 3...9 {
            let oldPos = currentPlayer.pos
            let oldScore = currentPlayer.score
            currentPlayer.moveForward(toAdd: outcome)
            let multiplier = chances[outcome]
            let wt = playDiracGames(player1: player1, player2: player2, player1Turn: !player1Turn, forPlayer1: forPlayer1, cache: &cache)
            winTimes.player1 += multiplier * wt.player1
            winTimes.player2 += multiplier * wt.player2
            currentPlayer.pos = oldPos
            currentPlayer.score = oldScore
        }

        cache[state] = winTimes

        return winTimes
    }

    func part2(data: [Player]) {
        var cache: [State: WinTimes] = [:]
        let wt = playDiracGames(player1: data[0], player2: data[1], player1Turn: true, forPlayer1: true, cache: &cache)
        print(max(wt.player1, wt.player2))
    }
}

Day21().solve()
