import AoCShared

class BingoLocation {
    let value: Int
    var used: Bool

    func markUsed() {
        used = true
    }

    init(value: Int) {
        self.value = value
        used = false
    }
}

extension BingoLocation: Hashable {
    static func ==(lhs: BingoLocation, rhs: BingoLocation) -> Bool {
        lhs.value == rhs.value
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }
}

class Board {
    let locations: [[BingoLocation]]

    var isWinner: Bool {
        for row in locations {
            if row.allSatisfy({ $0.used }) {
                return true
            }
        }

        for c in 0..<locations[0].count {
            if (0..<locations.count).allSatisfy({ locations[$0][c].used }) {
                return true
            }
        }

        return false
    }

    func markUsed(n: Int) {
        for row in locations {
            for item in row {
                if item.value == n {
                    item.markUsed()
                }
            }
        }
    }

    init(locations: [[BingoLocation]]) {
        self.locations = locations
    }
}

extension Board: CustomDebugStringConvertible {
    var debugDescription: String {
        var s = ""
        for row in locations {
            for item in row {
                if item.used {
                    s += "[" + String(item.value).padding(toLength: 2, withPad: " ", startingAt: 0) + "] "
                } else {
                    s += " " + String(item.value).padding(toLength: 2, withPad: " ", startingAt: 0) + "  "
                }
            }
            s += "\n"
        }

        return s
    }
}

extension Board: Hashable {
    static func ==(lhs: Board, rhs: Board) -> Bool {
        lhs.locations == rhs.locations
    }

    func hash(into hasher: inout Hasher) {
        for row in locations {
            for item in row {
                hasher.combine(item.value)
            }
        }
    }
}

class Bingo {
    let boards: [Board]
    let numbers: [Int]

    func markUsed(n: Int) {
        for board in boards {
            board.markUsed(n: n)
        }
    }

    var winner: Board? {
        boards.first {
            $0.isWinner
        }
    }

    var winners: Set<Board> {
        Set(boards.filter {
            $0.isWinner
        })
    }

    init(boards: [Board], numbers: [Int]) {
        self.boards = boards
        self.numbers = numbers
    }
}

extension Bingo: CustomDebugStringConvertible {
    var debugDescription: String {
        numbers.debugDescription + "\n\n" + boards.map {
            $0.debugDescription
        }.joined(separator: "\n")
    }
}

struct Day4: AdventOfCode {
    typealias InputData = Bingo

    func parse(input: String) -> Bingo? {
        let lines = input.split(whereSeparator: \.isNewline)
        let numbers = lines[0].split(separator: ",").compactMap {
            Int($0)
        }

        let boards = stride(from: 1, to: lines.count, by: 5).map { (l: Int) -> Board in
            let b = lines[l...l + 4]
            return Board(locations: b.map {
                $0.split(whereSeparator: \.isWhitespace).compactMap {
                    Int($0)
                }.map {
                    BingoLocation(value: $0)
                }
            })
        }

        return Bingo(boards: boards, numbers: numbers)
    }

    func part1(data: Bingo) {
        var i = 0
        while i < data.numbers.count {
            data.markUsed(n: data.numbers[i])
            if let w = data.winner {
                let c = w.locations.reduce(0) { count, row in
                    row.reduce(count) { c, item in
                        item.used ? c : c + item.value
                    }
                }
                print(c * data.numbers[i])
                break
            }
            i += 1
        }
    }

    func part2(data: Bingo) {
        var i = 0
        var prevWinners: Set<Board> = []
        while i < data.numbers.count {
            data.markUsed(n: data.numbers[i])
            if data.winners.count == data.boards.count {
                // Find the last winner
                if let w = data.winners.subtracting(prevWinners).first {
                    let c = w.locations.reduce(0) { count, row in
                        row.reduce(count) { c, item in
                            item.used ? c : c + item.value
                        }
                    }
                    print(c * data.numbers[i])
                    break
                }
            }
            prevWinners = data.winners
            i += 1
        }
    }
}

Day4().solve()
