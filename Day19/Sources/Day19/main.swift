import AoCShared
import Regex

struct Point: Equatable, Hashable {
    let x: Int
    let y: Int
    let z: Int

    static func +(lhs: Point, rhs: Point) -> Point {
        Point(x: lhs.x + rhs.x, y: lhs.y + rhs.y, z: lhs.z + rhs.z)
    }

    static func -(lhs: Point, rhs: Point) -> Point {
        Point(x: lhs.x - rhs.x, y: lhs.y - rhs.y, z: lhs.z - rhs.z)
    }
}

func distance(a: Point, b: Point) -> Int {
    abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z)
}

extension Point: CustomDebugStringConvertible {
    var debugDescription: String {
        "\(x),\(y),\(z)"
    }
}

struct Scanner: Equatable, Hashable {
    let index: Int
    var beacons: Set<Point>

    var allRotations: Set<[Point]> {
        var options: Set<[Point]> = []

        for xd in [1, -1] {
            for yd in [1, -1] {
                for zd in [1, -1] {
                    options.insert(beacons.map {
                        Point(x: $0.x * xd, y: $0.y * yd, z: $0.z * zd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.x * xd, y: $0.y * yd, z: $0.z * zd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.x * xd, y: $0.z * zd, z: $0.y * yd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.y * yd, y: $0.x * xd, z: $0.z * zd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.y * yd, y: $0.z * zd, z: $0.x * xd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.z * zd, y: $0.x * xd, z: $0.y * yd)
                    })
                    options.insert(beacons.map {
                        Point(x: $0.z * zd, y: $0.y * yd, z: $0.x * xd)
                    })
                }
            }
        }

        return options
    }
}

func inRange(center: Point, point: Point) -> Bool {
    abs(center.x - point.x) <= 1000 && abs(center.y - point.y) <= 1000 && abs(center.z - point.z) <= 1000
}

struct Day19: AdventOfCode {
    typealias InputData = [Scanner]

    func parse(input: String) -> [Scanner]? {
        var scanners: [Scanner] = []
        var lastScannerIndex = -1
        let scannerRegex = Regex("^--- scanner (\\d+) ---$")
        let positionRegex = Regex("^([0-9-]+),([0-9-]+),([0-9-]+)$")
        var currentBeacons: Set<Point> = []
        for line in input.split(whereSeparator: \.isNewline) {
            if let scannerIndexMatch = scannerRegex.firstMatch(in: String(line))?.captures,
               let scannerIndexString = scannerIndexMatch[0],
               let scannerIndex = Int(scannerIndexString) {
                if !currentBeacons.isEmpty {
                    scanners.append(Scanner(index: lastScannerIndex, beacons: currentBeacons))
                    currentBeacons = []
                }
                lastScannerIndex = scannerIndex
            } else if let captures = positionRegex.firstMatch(in: String(line))?.captures {
                guard let xs = captures[0],
                      let ys = captures[1],
                      let zs = captures[2],
                      let x = Int(xs),
                      let y = Int(ys),
                      let z = Int(zs) else {
                    return nil
                }
                currentBeacons.insert(Point(x: x, y: y, z: z))
            } else {
                return nil
            }
        }

        if !currentBeacons.isEmpty {
            scanners.append(Scanner(index: lastScannerIndex, beacons: currentBeacons))
            currentBeacons = []
        }

        return scanners
    }

    func compute(data: [Scanner]) -> (Set<Point>, [Point]) {
        var data = data
        // We place the first scanner at 0,0,0 without rotation, so we fix all these first points
        var fixedPoints: Set<Point> = []
        var scannerPositions: [Int: Point] = [:]
        scannerPositions[0] = Point(x: 0, y: 0, z: 0)
        for beacon in data[0].beacons {
            fixedPoints.insert(beacon)
        }

        // Now keep placing any of the beacons at any of the possible points
        while scannerPositions.count < data.count {
            let notPlaced = data.filter {
                scannerPositions[$0.index] == nil
            }
            for newScanner in notPlaced {
                var placed = false
                for (scannerIndex, oldCenter) in scannerPositions {
                    let scanner = data[scannerIndex]
                    for rotation in newScanner.allRotations {
                        // Try to match every point of the new scanner with every point of the old one
                        // If that results in >= 12 points matching (and none not), it fits
                        for newPoint in rotation {
                            for oldPoint in scanner.beacons {
                                let oldAbsolute = oldCenter + oldPoint
                                let newCenter = oldAbsolute - newPoint

                                var numMatched = 0
                                var matched: Set<[Point]> = []

                                for newScannerBeacon in rotation {
                                    let newScannerBeaconPosition = newScannerBeacon + newCenter
                                    if inRange(center: oldCenter, point: newScannerBeaconPosition) {
                                        // It is in range, check if the point also exists in the current scanner
                                        let pointRelativeToCenter = newScannerBeaconPosition - oldCenter
                                        if scanner.beacons.contains(pointRelativeToCenter) {
                                            numMatched += 1
                                            matched.insert([newScannerBeacon, pointRelativeToCenter])
                                        } else {
                                            // It does not exist, this position is invalid
                                            numMatched = -1
                                            break
                                        }
                                    }
                                    // If not in range, we ignore it since it won't do anything
                                }

                                if numMatched >= 12 {
                                    scannerPositions[newScanner.index] = newCenter
                                    for newScannerBeacon in rotation {
                                        let newScannerBeaconPosition = newScannerBeacon + newCenter
                                        fixedPoints.insert(newScannerBeaconPosition)
                                    }
                                    data[newScanner.index].beacons = Set(rotation)
                                    placed = true
                                    break
                                }
                            }
                            if placed {
                                break
                            }
                        }
                        if placed {
                            break
                        }
                    }
                }
                if placed {
                    break
                }
            }
        }

        return (fixedPoints, Array(scannerPositions.values))
    }

    func part1(data: [Scanner]) {
        print(compute(data: data).0.count)
    }

    func part2(data: [Scanner]) {
        let centers = compute(data: data).1
        var maxDist = Int.min
        for p1 in centers {
            for p2 in centers {
                maxDist = max(maxDist, distance(a: p1, b: p2))
            }
        }

        print(maxDist)
    }
}

Day19().solve()
