import AoCShared

struct Node: Equatable {
    var value: Int
    var depth: Int
}

struct Snailfish: Equatable {
    let nodes: [Node]

    init(nodes: [Node]) {
        var nodes = nodes
        while true {
            var stop = false
            for (idx, node) in nodes.enumerated() {
                // Explode
                if node.depth >= 5 {
                    // Left
                    if idx > 0 {
                        nodes[idx - 1].value += node.value
                    }
                    // Right
                    if idx < nodes.count - 2 {
                        nodes[idx + 2].value += nodes[idx + 1].value
                    }
                    nodes[idx] = Node(value: 0, depth: nodes.remove(at: idx).depth - 1)
                    stop = true
                    break
                }
            }

            if stop {
                continue
            }

            for (idx, node) in nodes.enumerated() {
                // Split
                if node.value >= 10 {
                    let x = node.value / 2
                    let y = node.value - x
                    nodes[idx] = Node(value: y, depth: node.depth + 1)
                    nodes.insert(Node(value: x, depth: node.depth + 1), at: idx)
                    stop = true
                    break
                }
            }

            if !stop {
                break
            }
        }

        self.nodes = nodes
    }

    static func +(lhs: Snailfish, rhs: Snailfish) -> Snailfish {
        if lhs.nodes == [] {
            return rhs
        } else if rhs.nodes == [] {
            return lhs
        }
        return Snailfish(nodes: (lhs.nodes + rhs.nodes).map {
            Node(value: $0.value, depth: $0.depth + 1)
        })
    }

    var magnitude: Int {
        // Keep finding two nodes that have the same level, since these
        // are the nodes that we can easily combine
        // We can 'misuse' value to get the magnitude
        var nodes = nodes
        var changed = true
        while changed {
            changed = false
            for i in 0..<nodes.count - 1 {
                if nodes[i].depth == nodes[i + 1].depth {
                    nodes[i] = Node(value: 3 * nodes.remove(at: i).value + 2 * nodes[i].value, depth: nodes[i].depth - 1)
                    changed = true
                    break
                }
            }
        }
        return nodes[0].value
    }
}

extension Snailfish: CustomDebugStringConvertible {
    var debugDescription: String {
        var s = ""
        for node in nodes {
            s += "\(node.value) <\(node.depth)> "
        }

        return s
    }
}

struct Day18: AdventOfCode {
    typealias InputData = [Snailfish]

    func parse(input: String) -> [Snailfish]? {
        var snailfishes: [Snailfish] = []
        for line in input.split(whereSeparator: \.isNewline) {
            var nodes: [Node] = []
            var curDepth = 0
            for c in line {
                if c == "[" {
                    curDepth += 1
                } else if c == "]" {
                    curDepth -= 1
                }

                if c.isNumber {
                    guard let value = Int(String(c)) else {
                        return nil
                    }
                    nodes.append(Node(value: value, depth: curDepth))
                }
            }

            snailfishes.append(Snailfish(nodes: nodes))
        }

        return snailfishes
    }

    func part1(data: [Snailfish]) {
        let sum = data.reduce(Snailfish(nodes: [])) {
            $0 + $1
        }
        print(sum.magnitude)
    }

    func part2(data: [Snailfish]) {
        var m = -1
        for s1 in data {
            for s2 in data {
                if s1 != s2 {
                    m = max(m, (s1 + s2).magnitude)
                }
            }
        }
        print(m)
    }
}

Day18().solve()
