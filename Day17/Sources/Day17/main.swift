import AoCShared
import Regex

struct Target {
    let minX: Int
    let maxX: Int
    let minY: Int
    let maxY: Int
}

struct Point: Hashable {
    let x: Int
    let y: Int

    static func +(lhs: Point, rhs: Point) -> Point {
        Point(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
}

struct Day17: AdventOfCode {
    typealias InputData = Target

    func parse(input: String) -> Target? {
        let r = Regex("^target area: x=([0-9-]+)..([0-9-]+), y=([0-9-]+)..([0-9-]+)$")

        guard let captures = r.firstMatch(in: input.trimmingCharacters(in: .whitespacesAndNewlines))?.captures,
              let minXS = captures[0],
              let maxXS = captures[1],
              let minYS = captures[2],
              let maxYS = captures[3],
              let minX = Int(minXS),
              let maxX = Int(maxXS),
              let minY = Int(minYS),
              let maxY = Int(maxYS) else {
            return nil
        }

        return Target(minX: minX, maxX: maxX, minY: minY, maxY: maxY)
    }

    func valid(data: Target, xVelocity: Int, yVelocity: Int) -> Bool {
        var xVelocity = xVelocity
        var yVelocity = yVelocity
        var p = Point(x: 0, y: 0)

        while true {
            p = p + Point(x: xVelocity, y: yVelocity)

            if p.x > data.maxX {
                // X overshoot, never gonna happen
                return false
            } else if p.x >= data.minX && p.x <= data.maxX {
                // Within x range, check y range
                if p.y < data.minY {
                    // Y overshoot, never gonna happen
                    return false
                } else if p.x >= data.minY && p.y <= data.maxY {
                    // Yay we are there
                    return true
                }
            }

            yVelocity -= 1
            if xVelocity > 0 {
                xVelocity -= 1
            }
        }
    }

    func getMaxVelocity(data: Target) -> Point
    {
        // First, find the x velocity. We do that by adding 1, 2, 3, .... until we are in the range
        var x = 0
        var toAdd = 1
        while x < data.minX {
            x += toAdd
            toAdd += 1
        }
        let xVelocity = toAdd - 1

        // Now find the best y velocity: binary search on all y values and see if they work
        var yVelocity = 0

        for y in min(data.minY, 0)...data.minX {
            if valid(data: data, xVelocity: xVelocity, yVelocity: y) {
                yVelocity = y
            }
        }

        return Point(x: xVelocity, y: yVelocity)
    }

    func part1(data: Target) {
        let maxVelocity = getMaxVelocity(data: data)
        let highest = (1 + maxVelocity.y) * maxVelocity.y / 2
        print(highest)
    }

    func part2(data: Target) {
        let maxVelocity = getMaxVelocity(data: data)

        // Now find all possibilities: for x it goes from maxVelocity.x to data.maxX
        // Then check all y's

        var allVelocities: Set<Point> = []
        for x in maxVelocity.x...data.maxX {
            for y in min(data.minY, 0)...maxVelocity.y {
                if valid(data: data, xVelocity: x, yVelocity: y) {
                    allVelocities.insert(Point(x: x, y: y))
                }
            }
        }

        print(allVelocities.count)
    }
}

Day17().solve()
