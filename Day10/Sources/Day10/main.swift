import AoCShared

struct Line {
    let data: [Character]

    // Note we don't really return the finish, but the open characters corresponding to them
    // We don't need the closing ones
    func firstCorruptedAndFinish() -> (corrupted: Character?, finish: [Character])? {
        var openStack: [Character] = []

        let mapping: [Character: Character] = [
            ")": "(",
            "]": "[",
            "}": "{",
            ">": "<",
        ]

        for c in data {
            if mapping.values.contains(c) {
                openStack.append(c)
            } else {
                // Use mapping to look it up
                guard let m = mapping[c] else {
                    return nil
                }
                guard let last = openStack.last else {
                    return nil
                }
                if last == m {
                    _ = openStack.popLast()
                } else {
                    return (corrupted: c, finish: [])
                }
            }
        }

        return (corrupted: nil, finish: openStack.reversed())
    }
}

struct Day10: AdventOfCode {
    typealias InputData = [Line]

    func parse(input: String) -> [Line]? {
        input.split(whereSeparator: \.isNewline).map {
            Line(data: [Character]($0))
        }
    }

    func part1(data: [Line]) {
        let points: [Character: Int] = [
            ")": 3,
            "]": 57,
            "}": 1197,
            ">": 25137,
        ]
        print(data.compactMap {
            $0.firstCorruptedAndFinish()
        }.compactMap {
            $0.corrupted
        }.compactMap {
            points[$0]
        }.reduce(0) {
            $0 + $1
        })
    }

    func score(line: [Character]) -> Int {
        let points: [Character: Int] = [
            "(": 1,
            "[": 2,
            "{": 3,
            "<": 4,
        ]

        return line.compactMap {
            points[$0]
        }.reduce(0) {
            $0 * 5 + $1
        }
    }

    func part2(data: [Line]) {
        let scores = data.compactMap {
                    $0.firstCorruptedAndFinish()
                }.filter {
                    $0.corrupted == nil
                }.map {
                    $0.finish
                }.map {
                    score(line: $0)
                }
                .sorted()

        print(scores[scores.count / 2])
    }
}

Day10().solve()
