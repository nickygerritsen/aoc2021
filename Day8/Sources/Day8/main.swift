import AoCShared

typealias Digits = Set<Character>

struct Entry {
    let patterns: [Digits]
    let output: [Digits]
}

struct Day8: AdventOfCode {
    typealias InputData = [Entry]

    func parse(input: String) -> [Entry]? {
        input.split(whereSeparator: \.isNewline).compactMap { line in
            let parts = line.split(separator: " ")
            let patterns = parts[0..<10].map {
                Set<Character>(String($0))
            }
            let output = parts[11..<15].map {
                Set<Character>(String($0))
            }

            return Entry(patterns: patterns, output: output)
        }
    }

    func part1(data: [Entry]) {
        let fixedLengths = Set([2, 3, 4, 7])
        let sum = data.reduce(0) { (count, entry) in
            entry.output.reduce(count) { (c, o) in
                if fixedLengths.contains(o.count) {
                    return c + 1
                }

                return c
            }
        }
        print(sum)
    }

    func findMapping(entry: Entry) -> [Digits: Int] {
        var m = [Digits: Int]()
        // First, we find the four that are easy because they are unique
        guard let m1 = entry.patterns.first(where: { $0.count == 2 }) else {
            return m
        }
        guard let m4 = entry.patterns.first(where: { $0.count == 4 }) else {
            return m
        }
        guard let m7 = entry.patterns.first(where: { $0.count == 3 }) else {
            return m
        }
        guard let m8 = entry.patterns.first(where: { $0.count == 7 }) else {
            return m
        }
        m[m1] = 1
        m[m4] = 4
        m[m7] = 7
        m[m8] = 8

        // Now find the digits with 5 parts
        var withFive = Set(entry.patterns.filter {
            $0.count == 5
        })

        // First, we find the 5. We do this by 'subtracting' 4 from 1
        let fourMinusOne = m4.subtracting(m1)

        // Now to find the 5 we find the item from withFive that contains these two digits
        guard let m5 = withFive.first(where: { $0.intersection(fourMinusOne) == fourMinusOne }) else {
            return m
        }
        withFive.remove(m5)
        m[m5] = 5

        // The 3 is the one that contains all of m1
        guard let m3 = withFive.first(where: { $0.intersection(m1) == m1 }) else {
            return m
        }
        withFive.remove(m3)
        m[m3] = 3

        guard let m2 = withFive.first else {
            return m
        }
        m[m2] = 2

        // Now find the digits with 6 parts
        var withSix = Set(entry.patterns.filter {
            $0.count == 6
        })

        guard let m9 = withSix.first(where: { $0.intersection(m4) == m4 }) else {
            return m
        }

        withSix.remove(m9)
        m[m9] = 9

        guard let m0 = withSix.first(where: { $0.intersection(m1) == m1 }) else {
            return m
        }

        withSix.remove(m0)
        m[m0] = 0

        guard let m6 = withSix.first else {
            return m
        }
        m[m6] = 6

        return m
    }

    func getNumbers(entry: Entry) -> Int {
        let m = findMapping(entry: entry)
        let n = entry.output.compactMap {
            m[$0]
        }
        let nn = n.map {
            String($0)
        }
        guard let r = Int(nn.joined()) else {
            return -1
        }

        return r
    }

    func part2(data: [Entry]) {
        print(data.reduce(0) {
            $0 + getNumbers(entry: $1)
        })
    }
}

Day8().solve()
