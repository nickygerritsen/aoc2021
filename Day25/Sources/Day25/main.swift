import AoCShared

enum Item: Character {
    case EastCucumber = ">"
    case SouthCucumber = "v"
}

struct Point: Hashable {
    let x: Int
    let y: Int
}

struct Map {
    var items: [Point: Item]
    let maxX: Int
    let maxY: Int

    mutating func moveDirection(i: Item) -> Bool {
        var newItems: [Point: Item] = [:]

        for x in 0..<maxX {
            for y in 0..<maxY {
                let p = Point(x: x, y: y)
                if let current = items[p] {
                    if current != i {
                        if newItems[p] == nil {
                            newItems[p] = current
                        }
                        continue
                    }
                    let neighbourX: Int
                    let neighbourY: Int
                    if i == .EastCucumber {
                        neighbourX = (x + 1) % maxX
                        neighbourY = y
                    } else {
                        neighbourX = x
                        neighbourY = (y + 1) % maxY
                    }

                    let np = Point(x: neighbourX, y: neighbourY)

                    if items[np] == nil {
                        // If there is nothing at the new position, move it there
                        newItems[np] = current
                    } else {
                        // Otherwise, keep it at the current position
                        newItems[p] = current
                    }
                }
            }
        }

        let changed = items != newItems
        items = newItems
        return changed
    }

    mutating func move() -> Bool {
        let e = moveDirection(i: .EastCucumber)
        let s = moveDirection(i: .SouthCucumber)
        return e || s
    }
}

extension Map: CustomDebugStringConvertible {
    var debugDescription: String {
        var s = ""
        for y in 0..<maxY {
            for x in 0..<maxX {
                let p = Point(x: x, y: y)
                if let i = items[p] {
                    s += String(i.rawValue)
                } else {
                    s += "."
                }
            }
            s += "\n"
        }

        return s
    }
}

struct Day25: AdventOfCode {
    typealias InputData = Map

    func parse(input: String) -> Map? {
        let lines = input.split(whereSeparator: \.isNewline)
        let maxY = lines.count
        let maxX = lines[0].count
        var items: [Point: Item] = [:]

        for (y, line) in lines.enumerated() {
            for (x, c) in [Character](line).enumerated() {
                if let i = Item(rawValue: c) {
                    let p = Point(x: x, y: y)
                    items[p] = i
                }
            }
        }

        return Map(items: items, maxX: maxX, maxY: maxY)
    }

    func part1(data: Map) {
        var data = data
        var i = 1
        while true {
            let changed = data.move()
            if !changed {
                print(i)
                return
            }
            i += 1
        }
    }

    func part2(data: Map) {
        // Nothing
    }
}

Day25().solve()
