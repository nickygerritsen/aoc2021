import AoCShared

enum Operation {
    case forward(amount: Int)
    case down(amount: Int)
    case up(amount: Int)

    static func parse(line: String) -> Operation? {
        let parts = line.split(separator: " ")
        if parts.count != 2 {
            return nil
        }

        guard let amount = Int(parts[1]) else {
            return nil
        }

        switch parts[0] {
        case "forward":
            return .forward(amount: amount)
        case "down":
            return .down(amount: amount)
        case "up":
            return .up(amount: amount)
        default:
            return nil
        }
    }
}

struct Day2: AdventOfCode {
    typealias InputData = [Operation]

    func parse(input: String) -> [Operation]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            Operation.parse(line: String($0))
        }
    }

    func part1(data: [Operation]) {
        var position = 0
        var depth = 0
        data.forEach { operation in
            switch operation {
            case .forward(amount: let amount):
                position += amount
            case .down(amount: let amount):
                depth += amount
            case .up(amount: let amount):
                depth -= amount
            }
        }

        print(position * depth)
    }

    func part2(data: [Operation]) {
        var position = 0
        var depth = 0
        var aim = 0
        data.forEach { operation in
            switch operation {
            case .forward(amount: let amount):
                position += amount
                depth += (aim * amount)
            case .down(amount: let amount):
                aim += amount
            case .up(amount: let amount):
                aim -= amount
            }
        }

        print(position * depth)
    }
}

Day2().solve()
