import AoCShared
import Regex

struct Point {
    let x: Int
    let y: Int
}

extension Point: Hashable {}

enum Fold {
    case AlongY(n: Int)
    case AlongX(n: Int)
}

struct Paper {
    var dots: [Point: Point]
    var folds: [Fold]

    mutating func fold(fold: Fold) {
        switch fold {
        case .AlongX(let n):
            // Vertical fold: everything where x==n goes away, everything after folds on the beginning
            var newDots: [Point: Point] = [:]
            for dot in dots {
                if dot.key.x == n {
                    continue
                } else if dot.key.x < n {
                    newDots[dot.key] = dot.key
                } else {
                    let newDot = Point(x: 2 * n - dot.key.x, y: dot.key.y)
                    newDots[newDot] = newDot
                }
            }
            dots = newDots
        case .AlongY(let n):
            // Horizontal fold: everything where y==n goes away, everything after folds on the beginning
            var newDots: [Point: Point] = [:]
            for dot in dots {
                if dot.key.y == n {
                    continue
                } else if dot.key.y < n {
                    newDots[dot.key] = dot.key
                } else {
                    let newDot = Point(x: dot.key.x, y: 2 * n - dot.key.y)
                    newDots[newDot] = newDot
                }
            }
            dots = newDots
        }
    }

    func printGrid() {
        // First sort the dots: first by y, then by x
        let dots = dots.values.sorted { (d1, d2) in
            if d1.y != d2.y {
                return d1.y < d2.y
            }

            return d1.x < d2.x
        }

        guard let minX = dots.map({ $0.x }).min(),
              let minY = dots.map({ $0.y }).min(),
              let maxX = dots.map({ $0.x }).max(),
              let maxY = dots.map({ $0.y }).max()
                else {
            return
        }

        var currentDotIndex = 0
        for y in minY...maxY {
            for x in minX...maxX {
                if currentDotIndex < dots.count && dots[currentDotIndex].x == x && dots[currentDotIndex].y == y {
                    print("#", separator: "", terminator: "")
                    currentDotIndex += 1
                } else {
                    print(".", separator: "", terminator: "")
                }
            }
            print("", separator: "", terminator: "\n")
        }
    }
}

struct Day13: AdventOfCode {
    typealias InputData = Paper

    func parse(input: String) -> Paper? {
        let pointRegex = Regex("^(\\d+),(\\d+)$")
        let foldRegex = Regex("^fold along (x|y)=(\\d+)$")

        var points = [Point: Point]()
        var folds = [Fold]()

        for line in input.split(whereSeparator: \.isNewline) {
            if let pointMatch = pointRegex.firstMatch(in: String(line))?.captures {
                guard let xs = pointMatch[0],
                 let ys = pointMatch[1],
                      let x = Int(xs),
                      let y = Int(ys) else { return nil }
                let p = Point(x: x, y: y)
                points[p] = p
            } else if let foldMatch = foldRegex.firstMatch(in: String(line))?.captures {
                guard let direction = foldMatch[0],
                      let amounts = foldMatch[1],
                    let amount = Int(amounts) else {
                    return nil
                }

                if direction == "x" {
                    folds.append(.AlongX(n: amount))
                } else {
                    folds.append(.AlongY(n: amount))
                }
            } else {
                return nil
            }
        }

        return Paper(dots: points, folds: folds)
    }

    func part1(data: Paper) {
        var data = data
        data.fold(fold: data.folds[0])
        print(data.dots.count)
    }

    func part2(data: Paper) {
        var data = data
        for fold in data.folds {
            data.fold(fold: fold)
        }

        data.printGrid()
    }
}

Day13().solve()
